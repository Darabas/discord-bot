const song = require('./functionalities/song')
const prefix = process.env.PREFIX

const commands = {
    play: song.execute,
    skip: song.skip,
    stop: song.stop,
    list: song.list,
    remove: song.remove,
    clear: song.clear
}

module.exports = async function (msg) {
    if (msg.author.bot || !msg.content.startsWith(prefix)) return

    let args = msg.content.split(" ")
    const command = args.shift().substring(1) //get the 1 parameter and remove token

    if (commands[command]) {
        commands[command](msg, args)
    } else {
        return msg.channel.send('Invalid command')
    }
}