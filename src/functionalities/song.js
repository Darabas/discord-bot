const ytdl = require("ytdl-core")
const axios = require('axios')
const googleToken = process.env.GOOGLE_TOKEN

//variables
const queue = new Map()

this.execute = async function(msg, args) {
    const serverQueue = queue.get(msg.guild.id)

    const voiceChannel = msg.member.voice.channel
    if (!voiceChannel)
        return msg.channel.send("You need to be in a voice channel to play music!")
    
    const permissions = voiceChannel.permissionsFor(msg.client.user)
    
    if (!permissions.has("CONNECT") || !permissions.has("SPEAK"))
        return msg.channel.send("I need the permissions to join and speak in your voice channel!")

    if (args[0].indexOf('youtube.com') < 0) {
        let query = args.join('%20') //join with spaces
        console.log ({query})
        
        var res = await axios.get(`https://www.googleapis.com/youtube/v3/search?q=${query}&key=${googleToken}`)
    }

    const url = res ? res.data.items[0].id.videoId : args[0]
    console.log ({url})

    if (!url) 
        return msg.channel.send("No videos found! :pensive:")

    const songInfo = await ytdl.getInfo(url)
    const song = {
        title: songInfo.videoDetails.title,
        url: songInfo.videoDetails.video_url,
    }

    if (!serverQueue) {
        const queueConstruct = {
            textChannel: msg.channel,
            voiceChannel: voiceChannel,
            connection: null,
            songs: [],
            volume: 5,
            playing: true
        }

        queueConstruct.songs.push(song)

        queue.set(msg.guild.id, queueConstruct)

        try {
            var connection = await voiceChannel.join()
            queueConstruct.connection = connection
            play(msg.guild, queueConstruct.songs[0], queueConstruct)
        } catch (err) {
            console.log(err)
            queue.delete(msg.guild.id)
            return msg.channel.send(err)
        }
    } else {
        console.log ('adding song to queue')
        serverQueue.songs.push(song)
        return msg.channel.send(`${song.title} has been added to the queue!`)
    }
}

this.skip = function(msg) {
    const serverQueue = queue.get(msg.guild.id)

    if (!msg.member.voice.channel || msg.member.voice.channel !== serverQueue.voiceChannel)
        return msg.channel.send("You have to be in the same voice channel to stop the music!")
    
    if (!serverQueue)
        return msg.channel.send("There is no song that I could skip!")

    serverQueue.connection.dispatcher.end()
}

this.stop = function(msg) {
    const serverQueue = queue.get(msg.guild.id)
    if (!msg.member.voice.channel || msg.member.voice.channel !== serverQueue.voiceChannel)
        return msg.channel.send("You have to be in the same voice channel to stop the music!")
    
    if (!serverQueue)
        return msg.channel.send("There is no song that I could stop!")
    

    msg.channel.send("stopping!")
    
    serverQueue.songs = []
    serverQueue.connection.dispatcher.end()
}

this.list = function(msg) {
    const serverQueue = queue.get(msg.guild.id)

    if (!serverQueue)
        return msg.channel.send("There is no song on the queue!")

    let list = ''
    for (let x = 0; x < serverQueue.songs.length; x++) 
        list += `${x}: ${serverQueue.songs[x].title}; \n`
    
    return msg.channel.send('queue: \n' + list)
}

this.clear = function(msg) {
    const serverQueue = queue.get(msg.guild.id)
    if (!msg.member.voice.channel || msg.member.voice.channel !== serverQueue.voiceChannel)
        return msg.channel.send("You have to be in the same voice channel to clear the queue!")
    
    if (!serverQueue)
        return msg.channel.send("There is no song on the queue!")
    
    serverQueue.songs = [ serverQueue.songs[0] ]
    return msg.channel.send("queue clean!")
}

this.remove = function(msg) {
    const serverQueue = queue.get(msg.guild.id)
    if (!msg.member.voice.channel || msg.member.voice.channel !== serverQueue.voiceChannel)
        return msg.channel.send("You have to be in the same voice channel to remove a song!")
    
    if (!serverQueue || !serverQueue.songs)
        return msg.channel.send("There is no song on the queue!")

    const index = parseInt(msg.content.split(" ")[1])

    if (index > serverQueue.songs.length || index < 0 || isNaN(index))
        return msg.channel.send("Insert a valid index!")

    if (index == 0)
        this.skip(msg)
    
    serverQueue.songs.splice(index, 1)

    msg.channel.send('song successfully removed')

    return this.list(msg)
}

function play(guild, song, serverQueue) {
    if (!serverQueue)
        serverQueue = queue.get(guild.id)

    if (!song) {
        serverQueue.voiceChannel.leave()
        queue.delete(guild.id)
        return
    }

    const dispatcher = serverQueue.connection
        .play(ytdl(song.url))
        .on("finish", () => {
            serverQueue.songs.shift()
            play(guild, serverQueue.songs[0])
        })
        .on("error", error => console.error('dispatcher', error))
    dispatcher.setVolumeLogarithmic(serverQueue.volume / 5)
    serverQueue.textChannel.send(`Start playing: **${song.title}**`)
}
