const Discord = require("discord.js")
require('dotenv').config();

const client = new Discord.Client()
const commandHandler = require('./commands')

client.once("ready", () => {
  console.log("Ready!")
})

client.once("reconnecting", () => {
  console.log("Reconnecting!")
})

client.once("disconnect", () => {
  console.log("Disconnect!")
})

client.on("message", commandHandler)

client.login(process.env.BOT_TOKEN);
